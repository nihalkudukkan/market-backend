const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Owner = require('../../model/Owner')
const JWT_SECRET = "marketsecretforowner";
const ownerAuth = express.Router();

ownerAuth.post('/signup', async(req, res)=>{
    try {
        const {owner, password} = req.body;
        if (!owner || !password) {
            return res.status(400).json({error: "please provide all fields"})
        }
        // check for users
        let user = await Owner.findOne({owner})
        if (user) {
            return res.status(400).json({error: "user already exist"})
        }
        // password hashing
        let hashed = await bcrypt.hash(password, 12);
        // profile creating
        let response = await Owner.create({owner, password: hashed})
        return res.json({message: "Created"})
    } catch (error) {
        return res.status(400).json({error})
    }
})

ownerAuth.post('/signin', async(req, res)=>{
    try {
        const {owner, password} = req.body;
        if (!owner || !password) {
            return res.status(400).json({error: "please provide all fields"})
        }
        // user check
        let user = await Owner.findOne({owner})
        if(!user){
            return res.status(401).json({error: "Invalid Email or password"})
        }
        // password check
        let match = await bcrypt.compare(password, user.password)
        if (match) {
            const token = jwt.sign({_id: user._id}, JWT_SECRET);
            const {owner} = user;
            return res.json({token, user:{owner}})
        }
        else {
            return res.status(422).json({error:"Invalid Email or password"})
        }
    } catch (error) {
        return res.json({error: "Invalid Email or password"})
    }
})

ownerAuth.get('/check/:owner', async(req, res)=>{
    try {
        const owner = req.params.owner;
        const saved = await Owner.findOne({owner})
        if (!saved) {
            return res.status(400).json({error: "No user found"})
        }
        return res.json(saved)
    } catch (error) {
        return res.status(400).json({error: "Check route error"})
    }
})

module.exports = ownerAuth;