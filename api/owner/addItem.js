const express = require('express');
const addItem = express.Router();
const ownerToken = require('../../middlewares/owertoken');
const Item = require('../../model/Items')

addItem.post('/add', ownerToken, async(req, res)=>{
    try {
        let {name, price, image, count, info} = req.body;
        if (!name || !price || !image || !count) {
            return res.status(400).json({error: "please provide all fields"})
        }
 
        let message = await Item.create({name, price, image, count, owner: req.user, info})
        res.json({message})
    } catch (error) {
        res.status(400).json({error})
    }
})

addItem.put('/update', ownerToken, async(req, res)=>{
    try {
        let {id} = req.body;
        if (!id) {
            return res.status(400).json({error: "please provide all fields"})
        }
        await Item.findOneAndUpdate({_id: id}, req.body)
        res.json({message: "updated"})
    } catch (error) {
        res.status(400).json({error})
    }
})

addItem.get('/get', ownerToken, async(req, res)=>{
    try {
        // res.send(req.user.owner)
        let items = await Item.find({owner:req.user._id})
        res.json(items)
    } catch (error) {
        res.status(400).json({error})
    }
})

module.exports = addItem;