const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../../model/User')
const Cart = require('../../model/cart')
const JWT_SECRET = "marketsecret";
const userAuth = express.Router();

userAuth.post('/signup', async(req, res)=>{
    try {
        const {fname, lname, email, phone, password} = req.body;
        if (!fname || !lname || !email || !phone || !password) {
            return res.status(400).json({error: "please provide all fields"})
        }
        // check for users
        let user = await Promise.all([User.findOne({email}), User.findOne({phone})])
        if (user[0] || user[1]) {
            return res.status(400).json({error: "user already exist"})
        }

        // password hashing
        let hashed = await bcrypt.hash(password, 12);
        // profile creating
        const newuser = await User.create({fname, lname, email, phone, password: hashed})
        await Cart.create({user: newuser})

        return res.json({message: "Created"})
    } catch (error) {
        return res.status(400).json({error})
    }
})

userAuth.post('/signin', async(req, res)=>{
    try {
        const {data, password} = req.body;
        let phone;
        if (!data || !password) {
            return res.status(400).json({error: "please provide all fields"})
        }
        //check if its a number
        if (isNaN(data)) {
            phone = undefined;
        } else {
            phone = data;
        }
        
        // user check
        // let user = await User.findOne({email})
        let user = await Promise.all([User.findOne({email: data}), User.findOne({phone})])
        //check any data present
        if(!user[0] && !user[1]){
            return res.status(401).json({error: "Invalid Email or password"})
        }
        //identify which data
        let k;
        if (user[0]) {
            k=0;
        }
        else if (user[1]) {
            k=1;
        }
        // password check
        let match = await bcrypt.compare(password, user[k].password)
        if (match) {
            const token = jwt.sign({_id: user[k]._id}, JWT_SECRET);
            user[k].password= undefined;
            return res.json({token, user: user[k]})
        }
        else {
            return res.status(422).json({error:"Invalid Email or password"})
        }
    } catch (error) {
        return res.json({error: "Invalid Email or password"})
    }
})

module.exports = userAuth;