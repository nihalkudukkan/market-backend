const express = require('express');
const cartRoute = express.Router();
const Item = require('../../model/Items');
const Cart = require('../../model/cart')
const userToken = require('../../middlewares/userToken');
const cart = require('../../model/cart');

cartRoute.put('/add', userToken, async(req, res)=>{
    try {
        let {count, itemId} = req.body;
        if (!count || !itemId) {
            return res.status(400).json({error: "Provide item id and count"})
        }
        let same;
        let item = await Item.findById(itemId)
        let oldCart = await Cart.findOne({user: req.user})
        oldCart.items.map(obj=>{
            if (obj.id==itemId) {
                same=obj;
            }
        })
        if (same) {
            await Cart.findOneAndUpdate({user:req.user}, {
                    $pull:{items: same}
                })
        }
        await Cart.findOneAndUpdate({user:req.user}, {
                    $push:{items: {id: item, count}}
                })
        return res.json({message: "Added to cart"})
    } catch (error) {
        return res.status(400).status(400).json({error: "Cart add failed"})
    }
})

cartRoute.put('/remove', userToken, async(req, res)=>{
    try {
        let {itemId} = req.body;
        if (!itemId) {
            return res.status(400).json({error:"Please provide item id"})
        }
        let same;
        let oldCart = await Cart.findOne({user: req.user})
        oldCart.items.map(obj=>{
            if (obj.id==itemId) {
                same=obj;
            }
        })
        if (same) {
            await Cart.findOneAndUpdate({user:req.user}, {
                    $pull:{items: same}
                })
            return res.json({message: "Cart item removed"})
        }
        return res.json({message: "No item with the id"})
    } catch (error) {
        return res.status(400).json({error: "Removal failed"})
    }
})

cartRoute.get('/get', userToken, (req, res)=>{
    Cart.findOne({user: req.user})
        .then(cart=>res.json(cart))
        .catch(error=>res.status(400).json({error}))
})

cartRoute.get('/check', userToken, (req, res)=>{
    let {id} = req.query;
    if (!id) {
        res.status(400).json({error: "no id given"})
    }
    // u can also write "items.id: id"
    cart.findOne({user: req.user, items: { $elemMatch: {id}}})
        .then(data=>res.json(data))
        .catch(error=>res.status(400).json({error}))
})

module.exports = cartRoute;