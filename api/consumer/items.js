const express = require('express');
const route = express.Router();
const Item = require('../../model/Items')
const Owner = require('../../model/Owner')

route.get('/all', async(req, res)=>{
    try {
        let items = await Item.find()
        res.json(items)
    } catch (error) {
        res.status(400).json({error})
    }
})

route.get('/item', async(req, res)=>{
    try {
        let {id} = req.query;
        let owner;
        let items = await Item.findOne({_id: id})
        if (items) {
            let ownerInfo = await Owner.findById(items.owner)
            owner = ownerInfo.owner;
        }
        let {_id, count, price, image, info, name, sales} = items;
        res.json({_id, count, price, image, info, name, sales, owner})
    } catch (error) {
        res.status(400).json({error})
    }
})

// route.get('/item', (req, res)=>{
//     let {id} = req.query;
//     Item.findOne({_id: id})
//         .populate("owner","_id owner")
//         .then(result=>{res.json({result});console.log(result);})
//         .catch(error=>res.status(400).json({error}))
// })

module.exports = route