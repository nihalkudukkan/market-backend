const express = require('express');
const cors = require('cors')
const connectDB = require('./config/db');
const ownerAuth = require('./api/owner/ownerAuth');
const addItem = require('./api/owner/addItem');
const consumerItems = require('./api/consumer/items');
const userAuth = require('./api/consumer/userAuth');
const cartRoute = require('./api/consumer/cart');

const app = express();

connectDB();

app.use(cors({ origin: true, credentials: true }))
app.use(express.json());

app.get('/', (req, res)=>{
    res.send("Back end for Market.com")
})

app.use('/owner', ownerAuth)
app.use('/owner', addItem)
app.use('/consumer', consumerItems)
app.use('/user', userAuth)
app.use('/user/cart', cartRoute)

app.listen(process.env.PORT || 8080, ()=>{console.log("server started");})