const mongoose = require('mongoose');

const OwnerSchema = new mongoose.Schema({
  owner: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('owner', OwnerSchema);