const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema.Types;

const cartSchema = new mongoose.Schema({
    items:[
        {
            id: {
                type: ObjectId,
                ref: 'items'
            },
            count: {
                type: Number
            }
        }
    ],
    user: {
        type: ObjectId,
        ref: "users"
    }
})

module.exports = mongoose.model('cart', cartSchema);