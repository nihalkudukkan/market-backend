const mongoose = require('mongoose');
const {ObjectId} = mongoose.Schema.Types;

const ItemsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    count: {
        type: Number,
        required: true
    },
    sales: {
        type: Number,
        default: 0
    },
    image: {
        type: String,
        required: true
    },
    info: {
        type: String
    },
    owner: {
        type: ObjectId,
        ref: "owners"
    }
});

module.exports = mongoose.model('items', ItemsSchema);