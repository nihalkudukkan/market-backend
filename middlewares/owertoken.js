const jwt = require('jsonwebtoken');
const JWT_SECRET = "marketsecretforowner";
const mongoose = require('mongoose');
const Owner = require('../model/Owner');

module.exports = (req, res, next) => {
    const {authorization} = req.headers;
    if (!authorization) {
        return res.status(401).json({error:"you must be logged in"});
    }
    jwt.verify(authorization, JWT_SECRET, (err, payload)=>{
        if(err){
            return   res.status(401).json({error:"you must be logged in"})
        }
        const{_id} = payload;
        Owner.findById(_id)
            .then(user=>{
                user.password = undefined;
                req.user = user;
                next();
            })
    })
}